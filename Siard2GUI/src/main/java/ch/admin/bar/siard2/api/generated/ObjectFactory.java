//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.02.19 at 10:19:04 AM CET 
//


package ch.admin.bar.siard2.api.generated;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ch.admin.bar.siard2.api.generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ch.admin.bar.siard2.api.generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SiardArchive }
     * 
     */
    public SiardArchive createSiardArchive() {
        return new SiardArchive();
    }

    /**
     * Create an instance of {@link MessageDigestType }
     * 
     */
    public MessageDigestType createMessageDigestType() {
        return new MessageDigestType();
    }

    /**
     * Create an instance of {@link SchemasType }
     * 
     */
    public SchemasType createSchemasType() {
        return new SchemasType();
    }

    /**
     * Create an instance of {@link UsersType }
     * 
     */
    public UsersType createUsersType() {
        return new UsersType();
    }

    /**
     * Create an instance of {@link RolesType }
     * 
     */
    public RolesType createRolesType() {
        return new RolesType();
    }

    /**
     * Create an instance of {@link PrivilegesType }
     * 
     */
    public PrivilegesType createPrivilegesType() {
        return new PrivilegesType();
    }

    /**
     * Create an instance of {@link ViewsType }
     * 
     */
    public ViewsType createViewsType() {
        return new ViewsType();
    }

    /**
     * Create an instance of {@link UniqueKeyType }
     * 
     */
    public UniqueKeyType createUniqueKeyType() {
        return new UniqueKeyType();
    }

    /**
     * Create an instance of {@link CandidateKeysType }
     * 
     */
    public CandidateKeysType createCandidateKeysType() {
        return new CandidateKeysType();
    }

    /**
     * Create an instance of {@link RoutineType }
     * 
     */
    public RoutineType createRoutineType() {
        return new RoutineType();
    }

    /**
     * Create an instance of {@link ForeignKeysType }
     * 
     */
    public ForeignKeysType createForeignKeysType() {
        return new ForeignKeysType();
    }

    /**
     * Create an instance of {@link ReferenceType }
     * 
     */
    public ReferenceType createReferenceType() {
        return new ReferenceType();
    }

    /**
     * Create an instance of {@link RoleType }
     * 
     */
    public RoleType createRoleType() {
        return new RoleType();
    }

    /**
     * Create an instance of {@link ForeignKeyType }
     * 
     */
    public ForeignKeyType createForeignKeyType() {
        return new ForeignKeyType();
    }

    /**
     * Create an instance of {@link TableType }
     * 
     */
    public TableType createTableType() {
        return new TableType();
    }

    /**
     * Create an instance of {@link AttributesType }
     * 
     */
    public AttributesType createAttributesType() {
        return new AttributesType();
    }

    /**
     * Create an instance of {@link TypesType }
     * 
     */
    public TypesType createTypesType() {
        return new TypesType();
    }

    /**
     * Create an instance of {@link AttributeType }
     * 
     */
    public AttributeType createAttributeType() {
        return new AttributeType();
    }

    /**
     * Create an instance of {@link CheckConstraintsType }
     * 
     */
    public CheckConstraintsType createCheckConstraintsType() {
        return new CheckConstraintsType();
    }

    /**
     * Create an instance of {@link ParametersType }
     * 
     */
    public ParametersType createParametersType() {
        return new ParametersType();
    }

    /**
     * Create an instance of {@link TablesType }
     * 
     */
    public TablesType createTablesType() {
        return new TablesType();
    }

    /**
     * Create an instance of {@link ParameterType }
     * 
     */
    public ParameterType createParameterType() {
        return new ParameterType();
    }

    /**
     * Create an instance of {@link TypeType }
     * 
     */
    public TypeType createTypeType() {
        return new TypeType();
    }

    /**
     * Create an instance of {@link CheckConstraintType }
     * 
     */
    public CheckConstraintType createCheckConstraintType() {
        return new CheckConstraintType();
    }

    /**
     * Create an instance of {@link TriggersType }
     * 
     */
    public TriggersType createTriggersType() {
        return new TriggersType();
    }

    /**
     * Create an instance of {@link ColumnType }
     * 
     */
    public ColumnType createColumnType() {
        return new ColumnType();
    }

    /**
     * Create an instance of {@link SchemaType }
     * 
     */
    public SchemaType createSchemaType() {
        return new SchemaType();
    }

    /**
     * Create an instance of {@link ViewType }
     * 
     */
    public ViewType createViewType() {
        return new ViewType();
    }

    /**
     * Create an instance of {@link TriggerType }
     * 
     */
    public TriggerType createTriggerType() {
        return new TriggerType();
    }

    /**
     * Create an instance of {@link UserType }
     * 
     */
    public UserType createUserType() {
        return new UserType();
    }

    /**
     * Create an instance of {@link ColumnsType }
     * 
     */
    public ColumnsType createColumnsType() {
        return new ColumnsType();
    }

    /**
     * Create an instance of {@link FieldType }
     * 
     */
    public FieldType createFieldType() {
        return new FieldType();
    }

    /**
     * Create an instance of {@link PrivilegeType }
     * 
     */
    public PrivilegeType createPrivilegeType() {
        return new PrivilegeType();
    }

    /**
     * Create an instance of {@link FieldsType }
     * 
     */
    public FieldsType createFieldsType() {
        return new FieldsType();
    }

    /**
     * Create an instance of {@link RoutinesType }
     * 
     */
    public RoutinesType createRoutinesType() {
        return new RoutinesType();
    }

}
