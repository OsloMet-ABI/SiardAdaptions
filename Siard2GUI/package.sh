#!/usr/bin/env bash

# Rebuild the project first
mvn clean package

# Try to package everything as a single executable
# The executable will be in package/chestnut/chestnut
#javapackager -deploy -native -outdir %outputDirectory% -outfile
# %outputFileName% -srcdir %deploySrcDir% -appclass %appclass% -name %name% -title %title%
javapackager -deploy -native -outdir package -outfile siard -srcdir target -srcfiles SiardGui-0.0.1-SNAPSHOT-jar-with-dependencies.jar  -appclass ch.admin.bar.siard2.gui.SiardGui -name "Siard" -title "Siard OsloMet"


