# Siard2 GUI

The Zip64File codebase from the official repo has been cloned and adapted so 
that it is possible to create an installer for Windows so we can get the program
installed at OsloMet. A JavaFX GUI is added. 

## Maven 
A quick way to run the project using maven is to go to the project directory
and run the following:

     mvn clean && mvn install && java -jar target/SiardGui-0.0.1-SNAPSHOT-jar-with-dependencies.jar

## Build installation packages

On Linux/Mac you can run
   
    sh package.sh

from the main project directory. On windows you can run
   
    package.bat 

from the main project directory. This should create appropriate installers for
your platform. The installer will be in package\bundles when complete. 

## Dependencies
While a lot of dependencies are handled automatically the following are
dependencies you should download yourself: 

 - db2jcc.jar
 - sqljdbc.jar
 - ojdbc6.jar

Note. It is possible to download these from the original Siard github account
but I am not sure that it is allowed to redistribute proprietary jar files. 
Once you get your hands on a copy of the jar files they can be added to the
local maven repository with the following commands:

mvn install:install-file -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0.3 -Dpackaging=jar -Dfile=ojdbc6.jar -DgeneratePom=true
mvn install:install-file -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc -Dversion=4.1 -Dpackaging=jar -Dfile=sqljdbc41.jar -DgeneratePom=true
mvn install:install-file -DgroupId=com.ibm.db2.jcc -DartifactId=db2jcc4 -Dversion=4.21.29 -Dpackaging=jar -Dfile=db2jcc4.jar -DgeneratePom=true
