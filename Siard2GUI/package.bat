title Create Siard@OsloMet windows executable

call mvn clean package

rem Creating a installer for Siard
rem The installer will be in package\bundles when complete
javapackager -deploy -native -outdir package -outfile siard -srcdir target -srcfiles SiardGui-0.0.1-SNAPSHOT-jar-with-dependencies.jar  -appclass ch.admin.bar.siard2.gui.SiardGui -name "Siard" -title "Siard OsloMet" -Bwin.menuGroup="OsloMet-ABI" -Bvendor="OsloMet via Swiss Federal Archives"



