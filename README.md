# SiardAdaptions

This repository exists solely to identify the changes we make to the Siard 
[code](https://github.com/sfa-siard) that we use for teaching. Basically there 
are two sub projects here SiardGui and Zip64 that have been adapted. 

The adaptions are needed because OsloMet required self-installing packages when
creating a copy of SiardGui for student use.

SiardGui has been converted to a maven project and comes with a packager that
allows a user to create installation packages for various OSes. It consumes all
the various Siard sub-modules into a single code base. Ideally we really would
have implemented this as a multi-module maven project, but that is something we
may consider later. Another reason for making the project a maven project is to
expose the dependencies in Siard and identify which are freely available and 
which are not. In our approach you manually have to download the dependencies 
that are not freely available.

In our version of Zip64, we have added a GUI (JavaFX application) that can be
used by the students to unzip a Siard file. Later we might add support to create
a Siard file from the GUI as well. We also have copied other portions of the
Siard code base into our copy of Zip64.

Note: Please do not use this code base unless you are a student at OsloMet and
are using it as part of teaching activities. Please only use the Siard tool
distributed from their distribution [channels](https://github.com/sfa-siard).
Minor changes have been undertaken on the code! The code should *not* be seen as
compatible or interchangeable with the official code base!
