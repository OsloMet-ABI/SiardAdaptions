# Zip64File GUI

The Zip64File codebase from the official repo has been cloned and adapted so 
that it is possible to create an installer for Windows so we can get the program
installed at OsloMet. A JavaFX GUI is added. 

## Maven 
A quick way to run the project using maven is to go to the project directory
and run the following:

     mvn clean && mvn install && java -jar target/Zip64File-0.0.1-jar-with-dependencies.jar 

## Build installation packages

On Linux/Mac you can run
   
    sh package.sh

from the main project directory. On windows you can run
   
    package.bat 

from the main project directory. This should create appropriate installers for
your platform. The installer will be in package\bundles when complete. 
