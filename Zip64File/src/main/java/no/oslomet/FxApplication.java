package no.oslomet;

import ch.enterag.utils.cli.Arguments;
import ch.enterag.zip.zip64;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * Created by tsodring on 21/02/2019
 * Capturing out and err is taken from:
 * https://stackoverflow.com/questions/13841884/redirecting-system-out-to-a-textarea-in-javafx
 */
public class FxApplication
        extends Application {

    @Override
    public void start(final Stage stage) {

        stage.setTitle("Siard @ OsloMet. Extract contents of Siard file");
        stage.setWidth(580);
        stage.setHeight(300);

        final FileChooser fileChooser = new FileChooser();
        final Button openButton = new Button("Extract Siard file...");
        final TextArea out = new TextArea();

        out.setWrapText(true);
        out.setEditable(false);
        out.setPrefRowCount(8);
        Console console = new Console(out);
        PrintStream ps = new PrintStream(console, true);
        System.setOut(ps);
        System.setErr(ps);

        openButton.setOnAction(
                new EventHandler<ActionEvent>() {
                    public void handle(final ActionEvent e) {
                        File file = fileChooser.showOpenDialog(stage);
                        if (file != null) {
                            openFile(file);
                        }
                    }
                });

        final GridPane buttonGridPane = new GridPane();
        GridPane.setConstraints(openButton, 0, 1);
        buttonGridPane.setHgap(6);
        buttonGridPane.setVgap(6);
        buttonGridPane.getChildren().addAll(openButton);

        final GridPane textGridPane = new GridPane();
        GridPane.setConstraints(openButton, 0, 0);
        textGridPane.setHgap(6);
        textGridPane.setVgap(6);
        textGridPane.getChildren().addAll(out);

        final Pane rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(buttonGridPane, textGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));

        System.setOut(ps);
        System.setErr(ps);

        stage.setScene(new Scene(rootGroup));
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

    private void openFile(File file) {
        try {
            file.getCanonicalPath();
            file.getAbsolutePath();

            String fileToExtract = file.getAbsolutePath();
            String folderToExtract = file.getParent()+File.separator;

            String[] args = new String[]
                    {
                            "x",
                            "-d:" + folderToExtract,
                            fileToExtract
                    };

            zip64 z64 = new zip64(Arguments.newInstance(args));

        } catch (IOException ex) {
            Logger.getLogger(
                    FxApplication.class.getName()).log(
                    Level.SEVERE, null, ex
            );
        }
    }

    public static class Console
            extends OutputStream {

        private TextArea output;

        public Console(TextArea ta) {
            this.output = ta;
        }

        @Override
        public void write(int i) throws IOException {
            output.appendText(String.valueOf((char) i));
        }
    }
}
