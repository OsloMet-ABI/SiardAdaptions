title Create Siard@OsloMet windows executable

call mvn clean package

rem Creating a installer for Siard/Zip64File
rem The installer will be in package\bundles when complete
javapackager -deploy -native -outdir package -outfile siard -srcdir target -srcfiles Zip64File-0.0.1-jar-with-dependencies.jar -appclass no.oslomet.FxApplication -name "Siard-Zip64File" -title "Siard-Zip64File OsloMet" -Bwin.menuGroup="OsloMet-ABI" -Bvendor="OsloMet via Swiss Federal Archives"
