#!/usr/bin/env bash

# Rebuild the project first
mvn clean package

# Try to package everything as a single executable
# The executable will be in package/bundles
javapackager -deploy -native -outdir package -outfile siard -srcdir target -srcfiles Zip64File-0.0.1-jar-with-dependencies.jar -appclass no.oslomet.FxApplication -name "Siard/Zip64File" -title "Siard/Zip64File OsloMet"
